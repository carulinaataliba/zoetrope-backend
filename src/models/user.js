const DataTypes = require("sequelize")
const sequelize = require("../config/sequelize");

// Definição da model com os atributos
const User = sequelize.define('User', { 
    email :{
        type: DataTypes.STRING,
        allowNull: false
    
    },

    name: {
        type: DataTypes.STRING,
        allowNull: false
    },

    date_of_birth: {
        type: DataTypes.DATEONLY,
        allowNull: false
    },

    phone_number: {
        type: DataTypes.STRING,
        
    },

    gender: {
        type: DataTypes.STRING
    },

    address: {
        type: DataTypes.STRING
    },

    password: {
        type: DataTypes.STRING
     },

     timestamps = false




    
});

// Definição das relações

User.associate = function name(models) {
    User.hasMany(models.User);
    User.hasMany(models.Comment, {as:"comments",foreignKey:"commentId"});
    
    
}

module.exports = User;


const DataTypes = require("sequelize")
const sequelize = require("../config/sequelize");

// Definição da model com os atributos
const Media = sequelize.define('Media', { 


    name: {
        type: DataTypes.STRING,
        allowNull: false
    },

    Gender: {
        type: DataTypes.DATEONLY,
        allowNull: false
    },

    Time : {
        type: DataTypes.STRING,
        
    },

    Review: {
        type: DataTypes.STRING
    },

    Debut: {
        type: DataTypes.STRING
    },

    Share: {
        type: DataTypes.STRING
     },

     timestamps = false




    
});

// Definição das relações

Media.associate = function name(models) {
    Media.hasMany(models.Comment);
    
}

module.exports = User;
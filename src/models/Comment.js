const DataTypes = require("sequelize")
const sequelize = require("../config/sequelize");

// Definição da model com os atributos
const Comment = sequelize.define('Comment', { 
    user_id :{
        type: DataTypes.STRING,
        allowNull: false
    
    },

    media_id: {
        type: DataTypes.STRING,
        allowNull: false
    },

    content: {
        type: DataTypes.DATEONLY,
        allowNull: false
    },

    ID: {
        type: DataTypes.STRING,
        allowNull: false
    },

    Review: {
        type: DataTypes.STRING
    },

    Share: {
        type: DataTypes.STRING
    },

     timestamps = false




    
});

// Definição das relações

comment.associate = function name(models) {
    comment.hasOne(models.Media);
    comment.hasOne(models.User);
   
    
}

module.exports = comment;
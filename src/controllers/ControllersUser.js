const { response } = require ('express');
const User = require ('../models/user')
const create = (req,res) => {
try {
    const user = await User.create(req.body);
    return res.status(201).json ({message: "Usuário cadastrado com!", user: user});

}catch(err) {
    res.status(500).json ({error: err});
}
const index = async (req,res) => {
    try {
        const user = await User.findAll();
        return res.status(200).json({user});
    }catch (err) {
        return res.status(500).json({err});
    }
}

const destroy = async (req,res) => {
    const {id} = req.params;
    try {
        const deletedUser = await User.destroy ({where: {id:id}});
        if(deletedUser) {
            return res.status(200).json("Usuário deletado");
        }
        throw new Error();
    }catch (err) {
        return res.status(500).json({err});
    }
}

const show = async (req,res) => {
    const {id} = req.params;
    try {
        const user = await User.findBYPk(id);
        return res.status(200).json({user});
    }catch (err) {
        return res.status(500).json({err});
    }
}

const update = async (req,res) => {
    const {id} = req.params;
    try {
        const [updated] = await User.update(req.body, {where: {id: id}});
        if (updated) {
            const user = await User.findByPk (id) ;

        }
        throw new Error () ;
    }catch(err) {
        return res.status(500).json("Usuário não encontrado");
    }
}


module.exports = {
    index,
    destroy,
    show,
    create,
    update,
    
};

const addRelationRole = async (req,res) => {
    const {id} = req.params;
    try {
        const user = await User.findByPk(id);
        const role = await Role.findByPk(req.body.RoleId);
        await user.setRole(role);
        return res.status(200).json(user);
    }catch(err){
        return res.status(500).json({err});

    }
}

const removeRelationRole =async(req,res) => {
    const {id} = req.params;
    try {
        const user = await User.findByPk(id);
        await user.setRole(null);
        return res.status(200).json(user);
    }catch(err){
        return res.status(500).json(user) ;

    }
}
};

